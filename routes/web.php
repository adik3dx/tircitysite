<?php
$router = new \App\System\Router();
$router->get('/', 'IndexController@actionIndex');
$router->get('news', 'NewsViewController@actionNews');
$router->post('news/oper', 'NewsPerformController@actionOper');
$router->get('news/update', 'NewsViewController@actionUpdate');
$router->get('news/delete', 'NewsPerformController@actionDelete');
$router->get('enter', 'EnterViewController@actionEnter');
$router->get('NotFound', 'NotFound@process');
$router->post('NotFound', 'NotFound@process');
$router->post('enter/log', 'EnterExitController@actionLog');
$router->get('enter/exit', 'EnterExitController@actionExit');
$router->post('news', 'NewsViewController@actionNews');
$router->get('logs', 'ShowLogs@show');
$router->ajax('ajLogs','AjLogs@getContent');
