<?php
use App\System\Content;
use App\System\AMenu;

?>
<!DOCTYPE html>
<html>
<head>
    <title>Портал</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<div class="col-md-1">
    <?php
    echo AMenu::getMenu();
    ?>
</div>
<div class="col-md-10">
    <div class="menuBar">
        <ul class="listBar">
            <li><a href="/">Главная</a></li>
            <li><a href='/news'>Новости</a></li>
            <li><a href="#">События</a></li>
            <li><a href="#">Компании</a></li>
            <li><a href="#">О нас</a></li>
            <div class="logoImg">
                <img src="/images/logoTir.png">
            </div>
            <?php
            require '../views/enter/EnterButton.php';
            ?>
        </ul>
    </div>
    <div class="mainBody col-md-10">
        <?php
        require Content::getContent();
        ?>
    </div>
    <div class="footer col-md-12">
        <strong>Городской портал Тирасполя</strong><br>
        <strong>Powered by Edward Baranetsky</strong>
    </div>
</div>
<div class="col-md-1"></div>
<script style="text/javascript" src="/js/jquery.js"></script>
<script style="text/javascript" src="/js/PagHandler.js"></script>
</body>
</html>