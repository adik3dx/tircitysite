<div class="cNews">
    <div class="wrapCNews">
        <h3>Редактирование новостной ленты</h3>
        <form action="/news/oper" method="post" enctype="multipart/form-data">
            Категория:
            <select name="category">
                <option value="<?= $item['category'] ?>"><?= $item['category'] ?></option>
                <option value="Политика">Политика</option>
                <option value="Культура">Культура</option>
                <option value="Спорт">Спорт</option>
            </select>
            <br/><br>
            <div>Заголовок:</div>
            <textarea name="title" class="areaTitle">
                        <?= $item['title'] ?></textarea><br>
            Текст новости:<br/>
            <textarea name="description" cols="60" rows="8"><?= $item['description'] ?></textarea><br/>
            Источник:
            <input type="text" name="source" size="51%" value="<?= $item['source'] ?>"/><br/>
            <input type="file" name="image" accept="image/*" value="<?= $item['image'] ?>" class="inputF">
            <input type="hidden" name="id" value="<?= $item['id'] ?>">
            <input type="submit" value="Изменить" class="pbtn">
        </form>
    </div>
    <img src="<?= '/'.$item['image'] ?>" class="imageOut">
</div>