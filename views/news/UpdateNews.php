<?php
use App\Models\NewsViewModel;
$updNews = new NewsViewModel();
$id = abs((int) strip_tags(trim($id)));
$news = $updNews->getNewsById($id);
foreach ($news as $item) {
    require '../views/news/UpdateNews.tpl.php';
}