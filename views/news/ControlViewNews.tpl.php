<div class="cNews">
    <div class="wrapCNews">
        <h3>Редактирование новостной ленты</h3>
        <form action="/news/oper" method="post" enctype="multipart/form-data">
            Категория:
            <select name="category">
                <option>Выберите категорию</option>
                <option value="Политика">Политика</option>
                <option value="Культура">Культура</option>
                <option value="Спорт">Спорт</option>
            </select>
            <br/>
            <span>Заголовок:</span><input type="text" name="title" size="21%" placeholder="введите заголовок"><br>
            Текст новости:<br/>
            <textarea name="description" cols="60" rows="8"></textarea><br/>
            Источник:
            <input type="text" name="source" size="51%" placeholder="http://"/><br/>
            <input type="file" name="image" multiple accept="image/*" class="inputF">
            <input type="submit" value="Добавить" class="pbtn">
        </form>
    </div>
    <br>
    <div class="searchField">
        <div class="searchForm">
            <form action="" method="post">
                <input type="text" name="searchNews" size="72%">
                <input type="submit" value="поиск" class="controlsbtn">
            </form>
        </div>
    </div>
    <div class="viewNews">
        <?php
        foreach ($news as $item) {
            ?>
            <div class="wrapOneNews">
                <br>
                <strong>Категория: </strong><?= $item['category'] ?><br>
                <h3><?= $item['title'] ?></h3>
                <img src="<?= $item['image'] ?>" class="imageOut">
                <div class="descriOut"><?= nl2br($item['description']) ?></div>
                <br>
                <strong>Источник: </strong><a href="<?= $item['source'] ?>"><?= $item['source'] ?></a><br>
                <strong>Добавлено: </strong><?= date('d-m-y H:i:s', $item['datetime']) ?><br>
                <a href="news/delete/id/<?= $item['id'] ?>">Удалить</a>
                <a href="news/update/id/<?= $item['id'] ?>">Изменить</a><br><br>
            </div>
            <?php
        }
        ?>
    </div>
</div>