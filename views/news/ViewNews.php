<?php
/*$viewNews = new App\Models\NewsViewModel();
$news = '';
$lastNews = $viewNews->getLastNews();
if (!empty($_POST['searchNews'])) {
    $title = strip_tags(trim($_POST['searchNews']));
    $news = $viewNews->getNewsByTitle($title);
} else {
    $news = $viewNews->getNews();
}*/
?>
<div class="viewNews">
    <div class="lastNews col-md-3">
        <h3 class="h3conf">Последние <strong>новости</strong></h3>
        <?php
        foreach ($lastNews as $item) {
            ?>
            <label class="oneLastNews"><img src="<?= $item['image'] ?>" width="100%"
                                            class="col-md-4"><?= $item['title'] ?></label>
            <?php

        }
        ?>
    </div>
    <br>
    <div class="listNews col-md-8">
        <div class="searchField">
            <div class="searchForm">
                <form action="" method="post">
                    <input type="text" name="searchNews" size="100%">
                    <input type="submit" value="поиск" class="sbtn">
                </form>
            </div>
        </div>
        <?php
        foreach ($news as $item) {
            ?>
            <div class="wrapUserOneNews">
                <div class="contentOneNews">
                    <br>
                    <h3><?= $item['title'] ?></h3>
                    <img src="<?= $item['image'] ?>" class="userImageOut">
                    <div class="descriOut"><?= nl2br($item['description']) ?></div>
                    <br>
                    <strong>Источник: </strong><a href="<?= $item['source'] ?>"><?= $item['source'] ?></a><br>
                    <strong>Добавлено: </strong><?= date('d-m-y H:i:s', $item['datetime']) ?><br>
                </div>
            </div>
            <?php

        }
        ?>
    </div>
</div>
