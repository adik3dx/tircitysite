<?php
use App\System\Request;
use App\Models\NewsViewModel;
use App\System\Response;

$viewNews = new NewsViewModel();
if ((new Request)->getSession('Admin')) {
    //include '../views/news/ControlViewNews.php';
    //$view = 'ControlViewNews.tpl.php';
    if (!empty($_POST['searchNews'])) {
        $title = strip_tags(trim($_POST['searchNews']));
        $news = $viewNews->getNewsByTitle($title);
        //require_once $view;
        //Response::changeCurrent('ControlNews', $news);
        require '../views/news/ControlViewNews.tpl.php';
    } else {
        $news = $viewNews->getNews();
        //$news = $viewNews->getNewsByTitle('курбан');
        //require_once $view;
        //var_dump($news[0]);
        //Response::changeCurrent('ControlNews', $news);
        require '../views/news/ControlViewNews.tpl.php';
    }
} else {
    // \App\System\Response::move('ViewNews');
    $news = '';
    $lastNews = $viewNews->getLastNews();
    if (!empty($_POST['searchNews'])) {
        $title = strip_tags(trim($_POST['searchNews']));
        $news = $viewNews->getNewsByTitle($title);
    } else {
        $news = $viewNews->getNews();
    }
    require '../views/news/ViewNews.php';
}
