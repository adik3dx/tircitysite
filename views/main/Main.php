<?php
use App\System\MyORM;
USE Config\DB;
use App\System\Request;

/*
 * эксперементы
 */

interface InterfaceName
{
    public function setName($name);

    public function getName();
}

class Name implements InterfaceName
{
    private $name = 'somebody';

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}

class Hello
{
    protected $name;
    private $sql;
    private $myORM;
    private $db;
    private $request;

    public function __construct(InterfaceName $name)
    {
        $this->myORM = new MyORM();
        $this->db = DB::getInstance();
        $this->name = $name;
        $this->request = new Request();
    }

    public function sayHello()
    {

        echo $this->request->getHello();
    }

    /*    public static function newInstance(){
            return new self();
        }*/
    public function array($items)
    {
        $string = 'Select';
        $last_element = array_pop($items);
        foreach ($items as $item) {
            $string .= ' ' . $item . ',';
        }
        $string .= ' ' . $last_element;
        $this->sql = $string;
        return $this;
    }

    public function table($table)
    {
        $this->sql .= " FROM $table";
        return $this;
    }

    public function get()
    {
        echo $this->sql;
    }

    public function insert($table, $params, $values)
    {
        $this->sql = "INSERT INTO $table (";
        $params = explode(',', $params);
        $values = explode(',', $values);
        $holders = " VALUES(";
        $start_params = $params;
        $last_param = array_pop($params);
        foreach ($params as $param) {
            $this->sql .= ' ' . $param . ',';
            $holders .= ' :' . $param . ',';
        }
        $this->sql .= ' ' . $last_param . ") " . $holders . ' :' . $last_param . ")";
        $stmt = $this->db->prepare($this->sql);
        $array = [];
        for ($i = 0; $i < count($values); $i++) {
            $array[":" . $start_params[$i]] = $values[$i];
        }
        //$stmt->execute($array);
    }

    public function update($table, $params, $values, $index)
    {
        $this->sql = "UPDATE $table";
        $params = explode('|s|', $params);
        $values = explode('|s|', $values);
        $set = " SET";
        $array = [];
        $start_params = $params;
        $last_param = array_pop($params);
        foreach ($params as $param) {
            $set .= " " . trim($param) . "=:" . trim($param) . ',';
        }
        $set .= " " . trim($last_param . "=:" . trim($last_param));
        for ($i = 0; $i < count($values); $i++) {
            $array[":" . trim($start_params[$i])] = trim($values[$i]);
        }
        $array[":id"] = trim($index);

        $this->sql .= $set . " WHERE id =:id";
        $stmt = $this->db->prepare($this->sql);
        //$stmt->execute($array);
        var_dump($this->sql);
        echo "\n";
        var_dump($array);
    }
}

class ReflectionUtil
{
    static function getClassSource(ReflectionClass $class)
    {
        $path = $class->getFileName();
        $lines = @file($path);
        $from = $class->getStartLine();
        $to = $class->getEndLine();
        $len = $to - $from + 1;
        return implode(array_slice($lines, $from - 1, $len));
    }
}

abstract class CostStrategy
{
    abstract function cost(Lesson $lesson);

    abstract function chargeType();
}

class TimedCostStrategy extends CostStrategy
{
    function cost(Lesson $lesson)
    {
        return ($lesson->getDuration() * 5);
    }

    function chargeType()
    {
        return "Почасовая оплата";
    }
}

class FixedCostStrategy extends CostStrategy
{
    function cost(Lesson $lesson)
    {
        return 30;
    }

    function chargeType()
    {
        return "Фиксированная ставка";
    }
}

abstract class Lesson
{
    private $duration;
    private $costStrategy;

    function __construct($duration, CostStrategy $strategy)
    {
        $this->duration = $duration;
        $this->costStrategy = $strategy;
    }

    function cost()
    {
        return $this->costStrategy->cost($this);
    }

    function chargeType()
    {
        return $this->costStrategy->chargeType();
    }

    function getDuration()
    {
        return $this->duration;
    }
}

class Lecture extends Lesson
{

}

class Seminar extends Lesson
{

}

class UseLesson
{
    public function __construct()
    {
        $lessons[] = new Seminar(4, new TimedCostStrategy());
        $lessons[] = new Lecture(4, new FixedCostStrategy());
        foreach ($lessons as $lesson) {
            print "Плата за занятие {$lesson->cost()} ";
            print "Тип оплаты: {$lesson->chargeType()}\n";
        }
    }

}
abstract class Expression{
    private static $keycount = 0;
    private $key;
    abstract function interpret(InterpreterContext $context);
    function getKey(){
        if(!isset($this->key)){
            self::$keycount++;
            $this->key = self::$keycount;
        }
        return $this->key;
    }
}
class LiteralExpression extends Expression{
    private $value;
    function __construct($value)
    {
        $this->value=$value;
    }
    function interpret(InterpreterContext $context){
        $context->replace($this,$this->value);
    }
}
class InterpreterContext{
    private $expressionstore=[];
    function replace(Expression $exp, $value){
        $this->expressionstore[$exp->getKey()]=$value;
    }
    function lookup(Expression $exp){
        return $this->expressionstore[$exp->getKey()];
    }
}
class VariableExpression extends Expression {
    private $name;
    private $val;
    function __construct($name , $val=null){
        $this->name= $name;
        $this->val = $val;
    }
    function interpret( InterpreterContext $context){
        if(!is_null($this->val)){
            $context->replace($this,$this->val);
            $this->val= null;
        }
    }
    function setValue($value){
        $this->val=$value;
    }
    function getKey()
    {
        return $this->name;
    }
}
abstract class OperatorExpression extends Expression{
    protected $l_op;
    protected $r_op;
    function __construct( Expression $l_op, Expression $r_op)
    {
        $this->l_op=$l_op;
        $this->r_op=$r_op;
    }
    function interpret(InterpreterContext $context){
        $this->l_op->interpret($context);
        $this->r_op->interpret($context);
        $result_l = $context->lookup($this->l_op);
        $result_r = $context->lookup($this->r_op);
        $this->doInterpret($context, $result_l,$result_r);
    }
    protected abstract function doInterpret(InterpreterContext $context,$result_l, $result_r);
}
class EqualsExpression extends OperatorExpression {
    protected function doInterpret(InterpreterContext $context, $result_l, $result_r)
    {
     $context->replace($this, $result_l==$result_r);
    }
}
class BooleanOrExpression extends OperatorExpression {
    protected function doInterpret(InterpreterContext $context, $result_l, $result_r)
    {
     $context->replace($this,$result_l||$result_r);
    }
}
class BooleanAndExpression extends OperatorExpression {
    protected function doInterpret(InterpreterContext $context, $result_l, $result_r)
    {
     $context->replace($this,$result_l&&$result_r);
    }
}
abstract class Tile{
    abstract function getWealthFactor();
}
class Plains extends Tile{
    private $wealthfactor = 2;
    function getWealthFactor()
    {
        return $this->wealthfactor;
    }
}
class Forest extends Tile{
    private $wealthdecorator = 10;
    function getWealthFactor()
    {
        return $this->wealthdecorator;
    }
}
abstract class TileDecorator extends Tile {
    protected $tile;
    function __construct(Tile $tile)
    {
        $this->tile = $tile;
    }
}
class DiamondDecorator extends TileDecorator {
    function getWealthFactor()
    {
        return $this->tile->getWealthFactor()+2;
    }
}
class PollutionDecorator extends TileDecorator {
    function getWealthFactor()
    {
        return $this->tile->getWealthFactor() - 4;
    }
}
abstract class KitchenTool{
    abstract function WhatIsIt();
}
abstract class DecoratorTolls extends KitchenTool {
    protected $tool;
    function __construct(KitchenTool $tool){
        $this->tool = $tool;
    }
}
class Fork extends KitchenTool {
    private $nameToll = 'Fork';
    function WhatIsIt()
    {
        return $this->nameToll;
    }
}
class SilverDecorator extends DecoratorTolls {
    function WhatIsIt()
    {
        return 'Silver '.$this->tool->WhatIsIt();
    }
}
class RequestHelper{}
abstract class ProcessRequest{
    abstract function process(RequestHelper $helper);
}
class MainProcess extends ProcessRequest {
    function process(RequestHelper $helper)
    {
        print __CLASS__ . ": выполнение запроса\n";
    }
}
abstract class ProcessDecorator extends ProcessRequest {
    protected $proc;
    function __construct(ProcessRequest $pr)
    {
        $this->proc=$pr;
    }
}
class LogRequest extends ProcessDecorator {
    function process(RequestHelper $helper)
    {
        print __CLASS__ . ": регистрация запроса\n";
        $this->proc->process($helper);
    }
}
echo "<pre>";
/*$useles = new UseLesson();
$name = new Name();
$hello = new Hello($name);*/
$useles = new UseLesson();
$context = new InterpreterContext();
$literal = new LiteralExpression("Четыре");
$literal->interpret($context);
print $context->lookup($literal)."\n";
$ch = curl_init();
curl_setopt($ch , CURLOPT_URL, "");
curl_exec($ch);
/*$hello->sayHello('Anna');
echo "\n";
$hello->array(['круто','привет','look'])->table('news')->get();
echo "\n";*/
/*$hello->update('news','category|s|description|s|datetime','politika|s|somethingopolitike|s|13','12');
var_dump($_SESSION);*/
//App\System\Response::move('News');
//App\System\Response::forFan('News', ['blin' => "komom"]);

//print_r(ReflectionUtil::getClassSource(new ReflectionClass('Hello')));

$tile = new PollutionDecorator(
    new DiamondDecorator(
        new Forest()
    ));
//echo $tile->getWealthFactor();
$fork = new SilverDecorator(new Fork());
//echo $fork->WhatIsIt();
$process = new LogRequest(new MainProcess());
$process->process(new RequestHelper());
//echo file_get_contents('../views/admin/AMenu.php');