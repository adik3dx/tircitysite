<?php
namespace App\Controllers;
use App\System\Response;
use App\Controllers\IrregularRequest;
use App\Controllers\LogRequest;
use App\Controllers\DoSomething;
use App\Controllers\RequestHelper;

class NotFound extends IrregularRequest
{
    public function process()
    {
        $process = new DoSomething(new LogRequest(new RequestHelper));
        $process->process();
        require_once (new Response("NotFound"))->send();
    }

/*    public function notFound(){
        require_once (new Response("NotFound"))->send();
    }*/
}