<?php

namespace App\Controllers;

use App\Models\NewsViewModel;
use App\System\Response;

class NewsViewController
{
    public function actionNews()
    {
        //Response::move('News');
        require_once (new Response("News"))->send();
/*        $content = '../views/news/News.php';
        require_once '../views/index.tpl.php';*/
    }

    public function actionUpdate($id)
    {
        //$content = '';
        /*$updNews = new NewsViewModel();
        $id = abs((int) strip_tags(trim($id)));
        $news = $updNews->getNewsById($id);
/*        foreach ($news as $item) {
            $content = '../views/news/UpdateNews.tpl.php';
        }

        require_once "../views/index.tpl.php";*/
        //echo (htmlentities($content));
        //Response::moveWith('UpdateNews',$news);
        require_once (new Response("UpdateNews"))->send();
    }
}
