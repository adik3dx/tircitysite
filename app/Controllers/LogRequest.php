<?php
namespace App\Controllers;
use App\Controllers\IrReqDecorator;
use App\Models\LogRequestModel;
class LogRequest extends IrReqDecorator{
    public function process()
    {
        $logReq = new LogRequestModel();
        $method = $this->request->getMethod();
        $addr = $this->request->getADDR();
        $browser = $this->request->getBrowser();
        $uri = $this->request->getUri();
        $logReq->saveLog($method,$addr,$browser,$uri);
        $this->process->process();
    }
}