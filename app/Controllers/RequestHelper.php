<?php
namespace App\Controllers;
use App\Controllers\IrregularRequest;
class RequestHelper extends IrregularRequest{
    public function process()
    {
        /** This Class is only a cap.
         * It was done to prevent recursion and make
         * Decorator pattern work.
        */
    }
}