<?php
namespace App\Controllers;
use App\System\Response;
use App\System\Request;
use App\Controllers\NotFound;
class ShowLogs
{
    public function show()
    {
        if((new Request())->getSession('Admin'))
        require_once (new Response("logs"))->send();
        else (new NotFound())->process();
    }
}