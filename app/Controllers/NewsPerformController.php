<?php

namespace App\Controllers;

use App\Models\NewsPerformModel;
use App\Models\NewsViewModel;
use App\System\Request;

class NewsPerformController
{
    public function actionDelete($id)
    {
        $viewNews = new NewsViewModel();
        $rmNews = new NewsPerformModel();
        $id = abs((int)strip_tags(trim($id)));
        $getById = $viewNews->getImageById($id);
        $imagePath = '';
        foreach ($getById as $item) {
            $imagePath = $item['image'];
        }
        $rmNews->deleteNews($id);
        unlink($imagePath);
        header('location: /news');
    }

    public function actionOper()
    {
        $request = new \App\System\Request();
        if ($request->getMethod() == 'POST') {
            if (!empty($_POST['id'])) {
                $id = abs((int)strip_tags(trim($_POST['id'])));
                $category = strip_tags(trim($_POST['category']));
                $title = strip_tags(trim($_POST['title']));
                $description = strip_tags(trim($_POST['description']));
                $source = strip_tags(trim($_POST['source']));
                $image = '';
                $prevImage = '';
                $updNews = new NewsPerformModel();
                $viewNews = new NewsViewModel();
                $getById = $viewNews->getImageById($id);
                foreach ($getById as $item) {
                    $prevImage = $item['image'];
                }
                if (!empty($_FILES['image']['name'])) {
                    $image = self::loadFile($_FILES['image']);
                    unlink($prevImage);
                }
                if ($image == '') {
                    $image = $prevImage;
                }
                $updNews->updateNews($id, $category, $title, $description, $source, $image);
                header('location: /news');
            } else {
                $category = strip_tags(trim($_POST['category']));
                $title = strip_tags(trim($_POST['title']));
                $description = strip_tags(trim($_POST['description']));
                $source = strip_tags(trim($_POST['source']));
                $image = self::loadFile($_FILES['image']);
                $addNews = new NewsPerformModel();
                $addNews->saveNews($category, $title, $description, $source, $image);
                header('location: ../news');
            }
        }
    }

    private function loadFile($data)
    {
        $uploads_dir = 'images/';
        $file = pathinfo($data['name']);
        $basename = $file['basename'];
        $filename = $file['filename'];
        $ext = $file['extension'];
        $tmp_name = $data['tmp_name'];
        if (file_exists($uploads_dir . $file['basename'])) {
            $newFileName = self::generStr(2) . $filename . self::generStr(2) . '.' . $ext;
            if (file_exists($uploads_dir . $newFileName)) {
                $newFileName = self::generStr(2) . $filename . self::generStr(2) . '.' . $ext;
                rename($tmp_name, $uploads_dir . $newFileName);

                return $uploads_dir . $newFileName;
            } else {
                rename($tmp_name, $uploads_dir . $newFileName);

                return $uploads_dir . $newFileName;
            }
        } else {
            move_uploaded_file($tmp_name, $uploads_dir . $basename);

            return $uploads_dir . $basename;
        }
    }

    private function generStr($dig)
    {
        $str = 'aabbhhtuaiopwffgnhpoutrewqqqsxccffbvadrtyuyyeoplidfjvbncdmxxewqrsafeyihgofpl';
        $num1 = rand(0, strlen($str));
        $num2 = $dig;
        $subStr = substr($str, $num1, $num2);

        return $subStr;
    }
}
