<?php
namespace App\Controllers;
use App\Controllers\IrregularRequest;
abstract class IrReqDecorator extends IrregularRequest{
    protected $process;
    protected $request;
    public function __construct(IrregularRequest $request)
    {
        $this->request = new \App\System\Request();
        $this->process=$request;
    }
}