<?php

namespace App\Controllers;

use App\Models\UserPerformModel;
use App\Models\UserViewModel;
use App\System\Request;

class EnterExitController
{
    public static function __callStatic($method, $args)
    {
        if (empty($args))
            return call_user_func([new Request(), "$method"]);
        else
            return call_user_func_array([new Request(), "$method"], $args);
    }

    public function actionExit()
    {
        self::deleteSession('Admin');
        header('location: /enter');
    }

    public function actionLog()
    {
/*        $user = new UserPerformModel();
        $login = trim(strip_tags($_POST['login']));
        $pass = trim(strip_tags($_POST['pass']));
        $pass = password_hash($pass, PASSWORD_DEFAULT);
        $user->saveUser($login,$pass);*/
        if (self::getMethod() == 'POST') {
            $user = new UserViewModel();
            $login = trim(strip_tags($_POST['login']));
            $pass = trim(strip_tags($_POST['pass']));
            $verMass = $user->getUser($login);
            $verHash = '';
            foreach ($verMass as $items) {
                $verHash = $items['pass'];
            }
            if (password_verify($pass, $verHash)) {
                self::setSession('Admin', true);
                self::deleteSession('erLab');
                header('location: /');
            } else {
                self::setSession('erLab', 'Введённые данные не верны');
                header('location: /enter');
            }
        }
    }
}
