<?php

namespace App\Models;

use App\System\MyORM;
use Config\DB;

class UserPerformModel extends UserAbstractModel implements IUserPerformModel
{
    private $db = null;
    private $myORM = null;

    public function __construct()
    {
        parent::__construct();
        $this->myORM = new MyORM();
        $this->db = parent::getDb();
        if (empty($istab[0])) {
            $sql = 'CREATE TABLE users(id INTEGER PRIMARY KEY AUTO_INCREMENT,
                                        login TEXT,
                                        pass TEXT)
                                        CHARACTER SET utf8 COLLATE utf8_general_ci';
            $this->db->exec($sql);
        }
    }

    public function saveUser($login, $pass)
    {
        /* try {
             $sql = 'INSERT INTO users (login,pass) VALUES(:login,:pass)';
             $stmt = $this->db->prepare($sql);
             $stmt->bindParam(':login', $login);
             $stmt->bindParam(':pass', $pass);
             $stmt->execute();
         } catch (PDOException $e) {
             echo 'on line: '.$e->getLine().', happened error: '.$e->getMessage();
         }*/
        $this->myORM->insert('users', 'login|s|pass', "$login|s|$pass");
    }

    public function deleteUser()
    {
        // TODO: Implement deleteUser() method.
    }
}
