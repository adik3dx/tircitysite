<?php

namespace App\Models;

//use Config\DB;
//use PDO;
use App\System\MyORM;

class UserViewModel extends UserAbstractModel implements IUserViewModel
{
    //private $db = null;
    private $myORM = null;

    public function __construct()
    {
        parent::__construct();
        $this->myORM = new MyORM();
        //$this->db = parent::getDb();
        if (empty($istab[0])) {
            return false;
        }
    }

    public function getUser($login)
    {
        /*        $sql = 'SELECT pass FROM users WHERE login = :login';
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':login', $login);
                $stmt->execute();
                $array = [];
                foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
                    $array[] = $row;
                }

                return $array;*/
        return $this->myORM->select('pass')->table('users')->where('login', '=', $login);
    }
}
