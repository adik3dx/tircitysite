<?php

namespace App\Models;

interface IUserViewModel
{
    public function getUser($login);
}
