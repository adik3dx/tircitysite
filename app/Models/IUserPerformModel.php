<?php

namespace App\Models;

interface IUserPerformModel
{
    public function saveUser($login, $pass);

    public function deleteUser();
}
