<?php

namespace App\Models;

interface INewsViewModel
{
    public function getNews();

    public function getNewsByTitle($title);

    public function getLastNews();

    public function getNewsById($id);

    public function getImageById($id);
}
