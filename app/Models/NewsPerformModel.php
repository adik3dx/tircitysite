<?php

namespace App\Models;

use Config\DB;
use App\System\MyORM;

class NewsPerformModel extends NewsAbstractModel implements INewsPerformModel
{
    private $db = null;
    private $myORM = null;

    public function __construct()
    {
        parent::__construct();
        $this->myORM = new MyORM();
        $this->db = parent::getDb();
        if (empty($istab[0])) {
            $sql = 'CREATE TABLE news(id INTEGER PRIMARY KEY AUTO_INCREMENT,
                                        category TEXT,
                                        title TEXT,
                                        description TEXT,
                                        source TEXT,
                                        image TEXT,
                                        datetime INTEGER)';
            $this->db->exec($sql);
        }
    }

    public function saveNews($category, $title, $description, $source, $image)
    {
        /*        try {
                    $sql = 'INSERT INTO news (category,title,description,source,image,datetime)
                                          VALUES(:category,:title,:description,:source,:image,:datetime)';
                    $stmt = $this->db->prepare($sql);
                    $datetime = time();
                    $stmt->execute(array(':category' => $category, ':title' => $title, ':description' => $description,
                        ':source' => $source, ':image' => $image, ':datetime' => $datetime, ));
                } catch (PDOException $e) {
                    echo 'on line: '.$e->getLine().', happened error: '.$e->getMessage();
                }*/
        $datetime = time();
        $this->myORM->insert('news', 'category|s| title|s| description|s| source|s| image|s| datetime',
            "$category|s| $title|s| $description|s| $source|s| $image|s| $datetime");
    }

    public function deleteNews($id)
    {
        /*        $sql = 'DELETE FROM news
                        WHERE id =:id';
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':id', $id);
                if (!$stmt->execute()) {
                    return false;
                }*/
        $this->myORM->delete('news')->where('id', '=', $id);
    }

    public function updateNews($id, $category, $title, $description, $source, $image)
    {
        /*        try {
                    $sql = 'UPDATE news
                            SET category =:category,title=:title,description = :description,source=:source,image=:image
                            WHERE id =:id';
                    $stmt = $this->db->prepare($sql);
                    $stmt->execute(array(':category' => $category, ':title' => $title, ':description' => $description,
                        ':source' => $source, ':image' => $image, ':id' => $id, ));
                } catch (PDOException $e) {
                    echo 'on line: '.$e->getLine().', happened error: '.$e->getMessage();
                }*/
        $this->myORM->update('news', 'category|s|title|s|description|s|source|s|image',
            "$category|s|$title|s|$description|s|$source|s|$image", "$id");
    }
}
