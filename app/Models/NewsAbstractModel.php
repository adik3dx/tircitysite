<?php

namespace App\Models;

use Config\DB;
use PDO;

abstract class NewsAbstractModel
{
    private $db = null;

    public function getDb()
    {
        return $this->db;
    }

    public function __construct()
    {
        try {
            $this->db = DB::getInstance();
        } catch (PDOException $e) {
            echo 'on line: '.$e->getLine().', happened error: '.$e->getMessage();
        }
        $istab = $this->db->query("SHOW TABLES LIKE 'news'");
        $istab = $istab->fetch(PDO::FETCH_NUM);
    }
}
