<?php
namespace App\Models;

use Config\DB;
use PDO;
use App\System\MyORM;

class LogRequestModel implements ILogRequest
{
    private $db = null;
    private $myORM = null;
    public function __construct()
    {
        try {
            $this->db = DB::getInstance();
            $this->myORM = new MyORM();
        } catch (PDOException $e) {
            echo 'on line: '.$e->getLine().', happened error: '.$e->getMessage();
        }
        $istab = $this->db->query("SHOW TABLES LIKE 'RequestLogs'");
        $istab = $istab->fetch(PDO::FETCH_NUM);
        if (empty($istab[0])) {
            $sql = 'CREATE TABLE RequestLogs(id INTEGER PRIMARY KEY AUTO_INCREMENT,
                                        method TEXT,
                                        addr TEXT,
                                        browser TEXT,
                                        uri TEXT,
                                        datetime INTEGER)
                                        CHARACTER SET utf8 COLLATE utf8_general_ci;
                                        SET GLOBAL event_scheduler=ON;
                                        DROP PROCEDURE IF EXISTS deleterLogs;
                                        delimiter |
                                        CREATE PROCEDURE deleterLogs()
                                        BEGIN
                                            SELECT count(datetime) FROM requestlogs INTO @rowsnum;
                                            WHILE @rowsnum>100 DO 
                                                DELETE FROM requestlogs order by datetime limit 1; 
                                                SET @rowsnum = @rowsnum - 1;
                                            END WHILE;
                                        END|
                                        delimiter ;
                                        DROP EVENT clearLogEvent;
                                        CREATE EVENT clearLogEvent
                                        ON SCHEDULE EVERY 2 HOUR 
                                        DO call deleterLogs();';
            $this->db->exec($sql);
        }
    }

    public function saveLog($method, $addr, $browser, $uri)
    {
        $datetime = time();
        $this->myORM->insert('RequestLogs', 'method|s| addr|s| browser|s| uri|s| datetime',
            "$method|s| $addr|s| $browser|s| $uri|s| $datetime");
    }

    public function getLogs()
    {
        return $this->myORM->select('method,addr, browser, uri, datetime')->table('RequestLogs')
            ->sort('datetime')->get();
    }
}