<?php

namespace App\Models;

//use Config\DB;
use App\System\MyORM;

//use PDO as PDO;

class NewsViewModel extends NewsAbstractModel implements INewsViewModel
{
    //private $db = null;
    private $myORM = null;

    public function __construct()
    {
        parent::__construct();
        $this->myORM = new MyORM();
        //$this->db = parent::getDb();
        if (empty($istab[0])) {
            return false;
        }
    }

    /*    private function getArr($data)
        {
            $arr = [];
            while ($row = $data->fetchALL(PDO::FETCH_ASSOC)) {
                $arr = $row;
            }

            return $arr;
        }*/

    /*private function getPrepArr($data)
    {
        $arr = [];
        while ($row = $data->fetchALL(PDO::FETCH_ASSOC)) {
            $arr = $row;
        }
        return $arr;
    }*/
    public function getNews()
    {
        /*        $sql = 'SELECT id,category,title,description,source,image,datetime
                        FROM news
                        ORDER by datetime DESC';
                $res = $this->db->query($sql);
                if (!$res) {
                    return false;
                } else {
                    return self::getArr($res);
                }*/
        return $this->myORM->select('id,category,title,description,source,image,datetime')->table('news')
            ->sort('datetime')->get();
    }

    public function getImageById($id)
    {
        /*        try {
                    $sql = 'SELECT image
                            FROM news
                            WHERE id =:id';
                    $stmt = $this->db->prepare($sql);
                    $stmt->bindParam(':id', $id);
                    $stmt->execute();

                    return self::getArr($stmt);
                } catch (PDOException $e) {
                    echo 'on line: '.$e->getLine().', happened error: '.$e->getMessage();
                }*/
        return $this->myORM->select('image')->table('news')->where("id", "=", $id);

    }

    public function getNewsById($id)
    {
        /*        try {
                    $sql = 'SELECT id,category,title,description,source,image,datetime
                            FROM news
                            WHERE id =:id';
                    $stmt = $this->db->prepare($sql);
                    $stmt->bindParam(':id', $id);
                    $stmt->execute();

                    return self::getArr($stmt);
                } catch (PDOException $e) {
                    echo 'on line: '.$e->getLine().', happened error: '.$e->getMessage();
                }*/
        return $this->myORM->select('id,category,title,description,source,image,datetime')->table('news')
            ->where('id', '=', $id);
    }

    public function getNewsByTitle($title)
    {
        /*        $sql = 'SELECT id,category,title,description,source,image,datetime
                        FROM news
                        WHERE title LIKE :param
                        ORDER BY id DESC';
                $param = '%'.$title.'%';
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':param', $param);
                $stmt->execute();

                return self::getArr($stmt);*/
        return $this->myORM->select('id,category,title,description,source,image,datetime')->table('news')
            ->where('title', 'LIKE', '%' . $title . '%');
    }

    public function getLastNews()
    {
        /*        $sql = 'SELECT id, title,image
                      FROM news
                      ORDER BY id DESC LIMIT 3';
                $lastNews = $this->db->query($sql);

                return self::getArr($lastNews);*/
        return $this->myORM->select('id, title,image')->table('news')->sort('id')->limit(3)->get();
    }
}
