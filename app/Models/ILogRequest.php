<?php

namespace App\Models;

interface ILogRequest
{
    public function saveLog($method, $addr, $browser, $uri);

    public function getLogs();
}
