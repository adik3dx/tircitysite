<?php

namespace App\Models;

interface INewsPerformModel
{
    public function saveNews($category, $title, $description, $source, $image);

    public function deleteNews($id);

    public function updateNews($id, $category, $title, $description, $source, $image);
}
