<?php

namespace App;

use App\System\Router;

class App
{
    public function run()
    {
        $request = new \App\System\Request();
        $module = '';
        $action = '';
        $params = [];
        //echo $request->getUri();
        $url_path = parse_url($request->getUri(), PHP_URL_PATH);
        $uri_parts = explode('/', trim($url_path, ' /'));
        $module = array_shift($uri_parts);
        $action = array_shift($uri_parts);
        for ($i = 0; $i < count($uri_parts); ++$i) {
            $params[$uri_parts[$i]] = $uri_parts[++$i];
        }
            if (empty($params)) {
                //echo $request->getUri();
                //echo (new Router())->getRoute($request->getUri());
                $res = preg_split('/@/', (new Router())->getRoute($request->getUri()));
                //var_dump([__NAMESPACE__.'\\Controllers\\'.$res[0], $res[1]]);
                call_user_func([__NAMESPACE__ . '\\Controllers\\' . $res[0], $res[1]]);

            } else {
                $res = preg_split('/@/', (new Router())->getRoute(trim($module . '/' . $action, '/')));
                //var_dump([__NAMESPACE__.'\\Controllers\\'.$res[0], $res[1]]);
                call_user_func_array([__NAMESPACE__ . '\\Controllers\\' . $res[0], $res[1]], $params);
            }
    }
}
