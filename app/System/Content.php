<?php
namespace App\System;
class Content{
    private static $content;

    /**
     * @return mixed
     */
    public static function getContent()
    {
        return self::$content;
    }

    /**
     * @param mixed $content
     */
    public static function setContent($content)
    {
        self::$content = $content;
    }

}