<?php

namespace App\System;

class View
{
    private $template = '';

    public function __construct()
    {
        ob_start();
        echo '<pre>';
    }

    public function render()
    {
        echo '</pre>';
        ob_end_flush();
    }
}
