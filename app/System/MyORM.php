<?php
namespace App\System;

use Config\DB;
use PDO;

class MyORM
{
    private $db;
    private $sql;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    private function getArr($data)
    {
        $arr = [];
        while ($row = $data->fetchALL(PDO::FETCH_ASSOC)) {
            $arr = $row;
        }

        return $arr;
    }

    public function select($fields)
    {
        $fields = explode(',', $fields);
        $sql = "SELECT ";
        $last_element = array_pop($fields);
        foreach ($fields as $field) {
            $sql .= ' ' . trim($field) . ',';
        }
        $sql .= ' ' . trim($last_element);
        $this->sql = $sql;
        return $this;
    }

    public function table($table)
    {
        $this->sql .= " FROM $table";
        return $this;
    }

    /**
     * @field - field on which we sort our array
     * @method - method with which we sort array
     * DESC - by default
     */

    public function sort($field, $method = 'DESC')
    {
        $this->sql .= ' ORDER by ' . $field . ' ' . $method;
        return $this;
    }

    /**
     * @num - number of needed items
     */
    public function limit($num)
    {
        $num = (int)$num;
        $this->sql .= " LIMIT $num";
        return $this;
    }

    /**
     * @field - field of criteria
     * @operator - may be '=' or LIKE
     * @value - values of the field
     */
    public function where($field, $operator, $value)
    {
        $this->sql .= " WHERE $field $operator :field";
        $stmt = $this->db->prepare($this->sql);
        $stmt->bindParam(':field', $value);
        $stmt->execute();
        return self::getArr($stmt);
    }

    public function get()
    {
        $res = $this->db->query($this->sql);
        if (!$res) {
            return false;
        } else {
            return self::getArr($res);
        }
    }

    /**
     * @table - table in which to insert
     * @params - array of parameter's names
     * @values - array of parameter's values
     * split all params and values with '|s|'
     */
    public function insert($table, $params, $values)
    {
        $this->sql = "INSERT INTO $table (";
        $params = explode('|s|', $params);
        $values = explode('|s|', $values);
        $holders = " VALUES(";
        $start_params = $params;
        $last_param = array_pop($params);
        foreach ($params as $param) {
            $this->sql .= ' ' . trim($param) . ',';
            $holders .= ' :' . trim($param) . ',';
        }
        $this->sql .= ' ' . trim($last_param) . ") " . $holders . ' :' . trim($last_param) . ")";
        $stmt = $this->db->prepare($this->sql);
        $array = [];
        for ($i = 0; $i < count($values); $i++) {
            $array[":" . trim($start_params[$i])] = trim($values[$i]);
        }
        $stmt->execute($array);
    }

    /**
     * @table - table from which delete
     */
    public function delete($table)
    {
        $this->sql = "DELETE FROM $table";
        return $this;
    }

    /**
     * @table - table which needs updating
     * @params - array of parameter's names
     * @values - array of parameter's values
     * @index - index of field which needs to update
     * split all params and values with '|s|'
     */
    public function update($table, $params, $values, $index)
    {
        $this->sql = "UPDATE $table";
        $params = explode('|s|', $params);
        $values = explode('|s|', $values);
        $set = " SET";
        $array = [];
        $start_params = $params;
        $last_param = array_pop($params);
        foreach ($params as $param) {
            $set .= " " . trim($param) . "=:" . trim($param) . ',';
        }
        $set .= " " . trim($last_param . "=:" . trim($last_param));
        for ($i = 0; $i < count($values); $i++) {
            $array[":" . trim($start_params[$i])] = trim($values[$i]);
        }
        $array[":id"] = trim($index);

        $this->sql .= $set . " WHERE id =:id";
        $stmt = $this->db->prepare($this->sql);
        $stmt->execute($array);
    }
}