<?php
namespace App\System;
class AMenu
{
    private static $AMenu = '';

    /**
     * @return mixed
     */
    public static function getMenu()
    {
        return self::$AMenu;
    }

    /**
     * @param mixed $content
     */
    public static function setMenu($file)
    {
        if($file=='')
        self::$AMenu =$file;
        else self::$AMenu = file_get_contents($file);
    }
}