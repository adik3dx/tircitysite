<?php

namespace App\System;

class Router
{
    private $request;

    private static $get = [];
    private static $post = [];
    private static $ajax = [];
    /**
     * @return array
     */
    /* public function getRoutes()
     {
         return self::$routes;
     }*/

    /**
     * @param $uri
     *
     * @return bool
     */
    public function getRoute($uri)
    {

        $this->request = new Request();
        if (!$this->request->isAJAX()) {
            $method = 'get';
            if ($this->request->getMethod() == 'POST') {
                $method = 'post';
            }
            if (empty(self::$$method[$uri]))
                $uri = "NotFound";
            return self::$$method[$uri]; //?: self::$$method["NotFound"];
        } else {
            return self::$ajax[$uri];
        }
    }

    public function get($name, $value)
    {
        self::$get[$name] = $value;
    }

    public function post($name, $value)
    {
        self::$post[$name] = $value;
    }
    public function ajax($name, $value)
    {
        self::$ajax[$name] = $value;
    }
}
