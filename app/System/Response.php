<?php
namespace App\System;

use App\System\Content;
use App\System\Request;
use App\System\AMenu;

class Response
{
    private static $response = [];
    private $content = '';
    private $status;
    private $headers = [];

    public function __construct($content = '', $status = 200, $headers = [])
    {
        $this->content = $content;
        $this->status = $status;
        $this->headers = $headers;
    }

    public function send()
    {
        if (empty(self::$response[$this->content])) {
            if((new Request())->getSession('Admin'))
                AMenu::setMenu('../views/admin/AMenu.php');
            else AMenu::setMenu('');
            Content::setContent(self::$response['NotFound']);
            return '../views/index.tpl.php';
        } else {
            /*if (empty($this->headers)) {
                Content::setContent(self::$response[$this->content]);
            } else {
                $content = '';
                foreach ($this->headers as $item) {
                    $content = self::$response[$this->content];
                }
                Content::setContent($content);
            }*/
            if((new Request())->getSession('Admin'))
                AMenu::setMenu('../views/admin/AMenu.php');
            else AMenu::setMenu('');
            Content::setContent(self::$response[$this->content]);
            return '../views/index.tpl.php';
        }

    }
/*
    public static function move($page)
    {
        //$content = self::$response[$page];
        Content::setContent(self::$response[$page]);
        require_once '../views/index.tpl.php';
        //(new Content)->setContent(self::$response[$page]);
    }
    */
    public static function forFan($page, $list_arg = [])
    {
        if (!empty($list_arg)) {
            extract($list_arg);
            echo "$blin";
        }
    }
/*
    public static function moveWith($page, $params)
    {
        foreach ($params as $item) {
            // $content = self::$response[$page];
            Content::setContent(self::$response[$page]);
        }
        require_once '../views/index.tpl.php';
    }

    public static function changeCurrent($page, $params = null)
    {
        $page = self::$response[$page];
        $fill = $page[1];
        require_once $page[0];
    }*/

    public function setResponse($name, $value)
    {
        self::$response[$name] = $value;
    }
}