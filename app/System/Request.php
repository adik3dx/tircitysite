<?php

namespace App\System;

class Request
{
    /**
     * All request data.
     *
     * @var array
     */
    private $request = [];

    public function __construct()
    {
        $this->setRequest($_SERVER);
    }

    /**
     * @removes session's item
     */
    public function deleteSession($name)
    {
        unset($_SESSION[$name]);
    }

    /**
     * @get session's item
     */
    public function getSession($name)
    {
        if (!empty($_SESSION[$name]))
            return $_SESSION[$name];
        else return false;
    }

    /**
     * @set session's item
     */
    public function setSession($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getBrowser(){
        return $this->request['HTTP_USER_AGENT'];
    }
    public function getADDR(){
        return $this->request['REMOTE_ADDR'];
    }
    public function getUri()
    {
        if ($this->request['REQUEST_URI'] == '/') {
            return $this->request['REQUEST_URI'];
        } else {
            return trim($this->request['REQUEST_URI'], '/');
        }
    }
    public function isAJAX(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            return true;
        else return false;
    }

    public function getMethod()
    {
        return $this->request['REQUEST_METHOD'];
    }
}
