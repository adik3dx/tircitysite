<?php

namespace config;

use PDO;

class DB
{
    private static $db;
    const DB_Name = 'portaltir';
    const Host_Name = 'localhost';
    const user = 'root';
    const pass = '';

    public static function getInstance()
    {
        if (self::$db === null) {
            $host = self::Host_Name;
            $dbname = self::DB_Name;
            $link = "mysql:host=$host;dbname=$dbname;charset=utf8";
            self::$db = new PDO($link, self::user, self::pass);
        }

        return self::$db;
    }
}
