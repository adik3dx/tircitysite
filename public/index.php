<?php

session_start();
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../routes/web.php';
require_once __DIR__.'/../routes/FillResponse.php';
$app = new App\App();
$app->run();
